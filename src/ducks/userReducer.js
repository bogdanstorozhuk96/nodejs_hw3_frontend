import { call, put, takeEvery } from "redux-saga/effects";

import MyApi from "../api";

const myApi = new MyApi();

const LOAD_USER_START = "nodejs_hw2_frontend/user/LOAD_USER_START";
const LOAD_USER_REQUEST = "nodejs_hw2_frontend/user/LOAD_USER_REQUEST";
const LOAD_USER_SUCCESS = "nodejs_hw2_frontend/user/LOAD_USER_SUCCESS";
const LOAD_USER_FAILURE = "nodejs_hw2_frontend/user/LOAD_USER_FAILURE";

const CHANGE_PASSWORD_START = "nodejs_hw2_frontend/user/CHANGE_PASSWORD_START";
const CHANGE_PASSWORD_REQUEST =
  "nodejs_hw2_frontend/user/CHANGE_PASSWORD_REQUEST";
const CHANGE_PASSWORD_SUCCESS =
  "nodejs_hw2_frontend/user/CHANGE_PASSWORD_SUCCESS";
const CHANGE_PASSWORD_FAILURE =
  "nodejs_hw2_frontend/user/CHANGE_PASSWORD_FAILURE";

const DELETE_USER_START = "nodejs_hw2_frontend/user/DELETE_USER_START";
const DELETE_USER_REQUEST = "nodejs_hw2_frontend/user/DELETE_USER_REQUEST";
const DELETE_USER_SUCCESS = "nodejs_hw2_frontend/user/DELETE_USER_SUCCESS";
const DELETE_USER_FAILURE = "nodejs_hw2_frontend/user/DELETE_USER_FAILURE";

const UPLOAD_USER_IMAGE_START =
  "nodejs_hw2_frontend/user/UPLOAD_USER_IMAGE_START";
const UPLOAD_USER_IMAGE_REQUEST =
  "nodejs_hw2_frontend/user/UPLOAD_USER_IMAGE_REQUEST";
const UPLOAD_USER_IMAGE_SUCCESS =
  "nodejs_hw2_frontend/user/UPLOAD_USER_IMAGE_SUCCESS";
const UPLOAD_USER_IMAGE_FAILURE =
  "nodejs_hw2_frontend/user/UPLOAD_USER_IMAGE_FAILURE";

const initialState = {
  user: {},
  loading: false,
  error: null,
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOAD_USER_REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case LOAD_USER_SUCCESS:
      return {
        ...state,
        user: action.payload,
        loading: false,
        error: null,
      };
    case LOAD_USER_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };

    case CHANGE_PASSWORD_REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case CHANGE_PASSWORD_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
      };
    case CHANGE_PASSWORD_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };

    case DELETE_USER_REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case DELETE_USER_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
      };
    case DELETE_USER_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };

    case UPLOAD_USER_IMAGE_REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case UPLOAD_USER_IMAGE_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
      };
    case UPLOAD_USER_IMAGE_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    default:
      return state;
  }
};

export default userReducer;

const loadUserRequested = () => ({
  type: LOAD_USER_REQUEST,
});
const loadUserSuccess = (user) => ({
  type: LOAD_USER_SUCCESS,
  payload: user,
});
const loadUserError = (error) => ({
  type: LOAD_USER_FAILURE,
  payload: error,
});
export const loadUserStart = () => ({
  type: LOAD_USER_START,
});

const changePasswordRequested = () => ({
  type: CHANGE_PASSWORD_REQUEST,
});
const changePasswordSuccess = () => ({
  type: CHANGE_PASSWORD_SUCCESS,
});
const changePasswordError = (error) => ({
  type: CHANGE_PASSWORD_FAILURE,
  payload: error,
});
export const changePasswordStart = (passwords) => ({
  type: CHANGE_PASSWORD_START,
  payload: passwords,
});

const deleteUserRequested = () => ({
  type: DELETE_USER_REQUEST,
});
const deleteUserSuccess = () => ({
  type: DELETE_USER_SUCCESS,
});
const deleteUserError = (error) => ({
  type: DELETE_USER_FAILURE,
  payload: error,
});
export const deleteUserStart = () => ({
  type: DELETE_USER_START,
});

const uploadUserImageRequested = () => ({
  type: UPLOAD_USER_IMAGE_REQUEST,
});
const uploadUserImageSuccess = () => ({
  type: UPLOAD_USER_IMAGE_SUCCESS,
});
const uploadUserImageError = (error) => ({
  type: UPLOAD_USER_IMAGE_FAILURE,
  payload: error,
});
export const uploadUserImageStart = (image) => ({
  type: UPLOAD_USER_IMAGE_START,
  payload: image,
});

function* loadUserAsync(action) {
  try {
    yield put(loadUserRequested());
    const { user } = yield call(() => myApi.getUser());
    yield put(loadUserSuccess(user));
  } catch (error) {
    yield put(loadUserError(error));
  }
}

export function* watchLoadUser() {
  yield takeEvery(LOAD_USER_START, loadUserAsync);
}

function* changePasswordAsync(action) {
  const { newPassword, oldPassword } = action.payload;
  try {
    yield put(changePasswordRequested());
    yield call(() => myApi.changePassword(newPassword, oldPassword));
    yield put(changePasswordSuccess());
  } catch (error) {
    yield put(changePasswordError(error));
  }
}

export function* watchChangePassword() {
  yield takeEvery(CHANGE_PASSWORD_START, changePasswordAsync);
}

function* deleteUserAsync(action) {
  try {
    yield put(deleteUserRequested());
    yield call(() => myApi.deleteUser());
    yield put(deleteUserSuccess());
  } catch (error) {
    yield put(deleteUserError(error));
  }
}

export function* watchDeleteUser() {
  yield takeEvery(DELETE_USER_START, deleteUserAsync);
}

function* uploadUserImageAsync(action) {
  try {
    const image = action.payload;
    yield put(uploadUserImageRequested());
    yield call(() => myApi.uploadToImgur(image));
    yield put(uploadUserImageSuccess());
  } catch (error) {
    yield put(uploadUserImageError(error));
  }
}

export function* watchUploadUserImage() {
  yield takeEvery(UPLOAD_USER_IMAGE_START, uploadUserImageAsync);
}
