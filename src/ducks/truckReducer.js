import { call, put, takeEvery } from "redux-saga/effects";

import MyApi from "../api";

const myApi = new MyApi();

const TOGGLE_TRUCK_MODAL = "nodejs_hw2_frontend/truck/TOGGLE_TRUCK_MODAL";

const SELECT_TRUCK = "nodejs_hw2_frontend/trucks/SELECT_TRUCK";

const REMOVE_ALL_TRUCKS = "nodejs_hw2_frontend/trucks/REMOVE_ALL_TRUCKS";

const LOAD_TRUCKS_START = "nodejs_hw2_frontend/trucks/LOAD_TRUCKS_START";
const LOAD_TRUCKS_REQUEST = "nodejs_hw2_frontend/trucks/LOAD_TRUCKS_REQUEST";
const LOAD_TRUCKS_SUCCESS = "nodejs_hw2_frontend/trucks/LOAD_TRUCKS_SUCCESS";
const LOAD_TRUCKS_FAILURE = "nodejs_hw2_frontend/trucks/LOAD_TRUCKS_FAILURE";

const ADD_TRUCK_START = "nodejs_hw2_frontend/trucks/ADD_TRUCK_START";
const ADD_TRUCK_REQUEST = "nodejs_hw2_frontend/trucks/ADD_TRUCK_REQUEST";
const ADD_TRUCK_SUCCESS = "nodejs_hw2_frontend/trucks/ADD_TRUCK_SUCCESS";
const ADD_TRUCK_FAILURE = "nodejs_hw2_frontend/trucks/ADD_TRUCK_FAILURE";

const DELETE_TRUCK_START = "nodejs_hw2_frontend/trucks/DELETE_TRUCK_START";
const DELETE_TRUCK_REQUEST = "nodejs_hw2_frontend/trucks/DELETE_TRUCK_REQUEST";
const DELETE_TRUCK_SUCCESS = "nodejs_hw2_frontend/trucks/DELETE_TRUCK_SUCCESS";
const DELETE_TRUCK_FAILURE = "nodejs_hw2_frontend/trucks/DELETE_TRUCK_FAILURE";

const UPDATE_TRUCK_START = "nodejs_hw2_frontend/trucks/UPDATE_TRUCK_START";
const UPDATE_TRUCK_REQUEST = "nodejs_hw2_frontend/trucks/UPDATE_TRUCK_REQUEST";
const UPDATE_TRUCK_SUCCESS = "nodejs_hw2_frontend/trucks/UPDATE_TRUCK_SUCCESS";
const UPDATE_TRUCK_FAILURE = "nodejs_hw2_frontend/trucks/UPDATE_TRUCK_FAILURE";

const ASSIGN_TRUCK_START = "nodejs_hw2_frontend/trucks/ASSIGN_TRUCK_START";
const ASSIGN_TRUCK_REQUEST = "nodejs_hw2_frontend/trucks/ASSIGN_TRUCK_REQUEST";
const ASSIGN_TRUCK_SUCCESS = "nodejs_hw2_frontend/trucks/ASSIGN_TRUCK_SUCCESS";
const ASSIGN_TRUCK_FAILURE = "nodejs_hw2_frontend/trucks/ASSIGN_TRUCK_FAILURE";

const initialState = {
  selectedTruck: null,
  trucks: [],
  loading: false,
  error: null,
  showTrucksModal: false,
};

const truckReducer = (state = initialState, action) => {
  switch (action.type) {
    case TOGGLE_TRUCK_MODAL:
      return {
        ...state,
        showTrucksModal: !state.showTrucksModal,
      };
    case SELECT_TRUCK:
      return {
        ...state,
        selectedTruck: action.payload,
      };
    case REMOVE_ALL_TRUCKS:
      return {
        ...state,
        trucks: [],
      };
    case LOAD_TRUCKS_REQUEST:
      return {
        ...state,
        trucks: [],
        loading: true,
        error: null,
      };
    case LOAD_TRUCKS_SUCCESS:
      return {
        ...state,
        trucks: action.payload,
        loading: false,
        error: null,
      };
    case LOAD_TRUCKS_FAILURE:
      return {
        ...state,
        trucks: [],
        loading: false,
        error: action.payload,
      };

    case ADD_TRUCK_REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case ADD_TRUCK_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
      };
    case ADD_TRUCK_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };

    case DELETE_TRUCK_REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case DELETE_TRUCK_SUCCESS:
      const index = state.trucks.findIndex(({ _id }) => _id === action.payload);
      return {
        ...state,
        trucks: [
          ...state.trucks.slice(0, index),
          ...state.trucks.slice(index + 1),
        ],
        loading: false,
        error: null,
      };
    case DELETE_TRUCK_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };

    case UPDATE_TRUCK_REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case UPDATE_TRUCK_SUCCESS:
      const { trucks } = state;
      const truck = action.payload;
      const truckIndex = trucks.findIndex(({ _id }) => _id === truck._id);
      return {
        ...state,
        trucks: [
          ...trucks.slice(0, truckIndex),
          truck,
          ...trucks.slice(truckIndex + 1),
        ],
        loading: false,
        error: null,
      };
    case UPDATE_TRUCK_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };

    case ASSIGN_TRUCK_REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case ASSIGN_TRUCK_SUCCESS:
      const { assignTruckId, userId } = action.payload;
      let resultTrucksAfterAssignment = [...state.trucks];
      const truckToBeDeassignedIndex = state.trucks.findIndex(
        ({ assigned_to }) => assigned_to
      );
      if (truckToBeDeassignedIndex !== -1) {
        resultTrucksAfterAssignment = [
          ...resultTrucksAfterAssignment.slice(0, truckToBeDeassignedIndex),
          {
            ...resultTrucksAfterAssignment[truckToBeDeassignedIndex],
            assigned_to: null,
          },
          ...resultTrucksAfterAssignment.slice(truckToBeDeassignedIndex + 1),
        ];
      }
      const truckToBeAssignedIndex = resultTrucksAfterAssignment.findIndex(
        ({ _id }) => _id === assignTruckId
      );
      resultTrucksAfterAssignment = [
        ...resultTrucksAfterAssignment.slice(0, truckToBeAssignedIndex),
        {
          ...resultTrucksAfterAssignment[truckToBeAssignedIndex],
          assigned_to: userId,
        },
        ...resultTrucksAfterAssignment.slice(truckToBeAssignedIndex + 1),
      ];
      return {
        ...state,
        trucks: resultTrucksAfterAssignment,
        loading: false,
        error: null,
      };
    case ASSIGN_TRUCK_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    default:
      return state;
  }
};

export default truckReducer;

export const toggleTrucksModal = () => ({
  type: TOGGLE_TRUCK_MODAL,
});

export const selectTruck = (truck) => ({
  type: SELECT_TRUCK,
  payload: truck,
});

export const removeAllTrucks = () => ({
  type: REMOVE_ALL_TRUCKS,
});
const trucksLoadSuccess = (newTrucks) => ({
  type: LOAD_TRUCKS_SUCCESS,
  payload: newTrucks,
});
const trucksLoadRequested = () => ({
  type: LOAD_TRUCKS_REQUEST,
});
const trucksLoadError = (error) => ({
  type: LOAD_TRUCKS_FAILURE,
  payload: error,
});
export const trucksLoadStart = () => ({
  type: LOAD_TRUCKS_START,
});

const addTruckSuccess = () => ({
  type: ADD_TRUCK_SUCCESS,
});
const addTruckRequested = () => ({
  type: ADD_TRUCK_REQUEST,
});
const addTruckError = (error) => ({
  type: ADD_TRUCK_FAILURE,
  payload: error,
});
export const addTruckStart = (newTruck) => ({
  type: ADD_TRUCK_START,
  payload: newTruck,
});

const deleteTruckSuccess = (_id) => ({
  type: DELETE_TRUCK_SUCCESS,
  payload: _id,
});
const deleteTruckRequested = () => ({
  type: DELETE_TRUCK_REQUEST,
});
const deleteTruckError = (error) => ({
  type: DELETE_TRUCK_FAILURE,
  payload: error,
});
export const deleteTruckStart = (_id) => ({
  type: DELETE_TRUCK_START,
  payload: _id,
});

const updateTruckSuccess = (updatedTruck) => ({
  type: UPDATE_TRUCK_SUCCESS,
  payload: updatedTruck,
});
const updateTruckRequested = () => ({
  type: UPDATE_TRUCK_REQUEST,
});
const updateTruckError = (error) => ({
  type: UPDATE_TRUCK_FAILURE,
  payload: error,
});
export const updateTruckStart = (updatedTruck) => ({
  type: UPDATE_TRUCK_START,
  payload: updatedTruck,
});

const assignTruckSuccess = (assignTruckData) => ({
  type: ASSIGN_TRUCK_SUCCESS,
  payload: assignTruckData,
});
const assignTruckRequested = () => ({
  type: ASSIGN_TRUCK_REQUEST,
});
const assignTruckError = (error) => ({
  type: ASSIGN_TRUCK_FAILURE,
  payload: error,
});
export const assignTruckStart = (assignTruckId, userId) => ({
  type: ASSIGN_TRUCK_START,
  payload: { assignTruckId, userId },
});

function* fetchTrucksAsync(action) {
  try {
    yield put(trucksLoadRequested());
    const data = yield call(() => myApi.getTrucks());
    yield put(trucksLoadSuccess(data[0].trucks));
  } catch (error) {
    yield put(trucksLoadError(error));
  }
}

export function* watchFetchTrucks() {
  yield takeEvery(LOAD_TRUCKS_START, fetchTrucksAsync);
}

function* addTruckAsync(action) {
  try {
    const newTruck = action.payload;
    yield put(addTruckRequested());
    yield call(() => myApi.addTruck(newTruck));
    yield put(addTruckSuccess());
    yield put(trucksLoadStart());
  } catch (error) {
    yield put(addTruckError(error));
  }
}

export function* watchAddTruck() {
  yield takeEvery(ADD_TRUCK_START, addTruckAsync);
}

function* deleteTruckAsync(action) {
  try {
    const _id = action.payload;
    yield put(deleteTruckRequested());
    yield call(() => myApi.deleteTruckById(_id));
    yield put(deleteTruckSuccess(_id));
  } catch (error) {
    yield put(deleteTruckError(error));
  }
}

export function* watchDeleteTruck() {
  yield takeEvery(DELETE_TRUCK_START, deleteTruckAsync);
}

function* updateTruckAsync(action) {
  try {
    const updatedTruck = action.payload;
    yield put(updateTruckRequested());
    yield call(() => myApi.updateTruckById(updatedTruck));
    yield put(updateTruckSuccess(updatedTruck));
  } catch (error) {
    yield put(updateTruckError(error));
  }
}

export function* watchUpdateTruck() {
  yield takeEvery(UPDATE_TRUCK_START, updateTruckAsync);
}

function* assignTruckAsync(action) {
  try {
    const { assignTruckId, userId } = action.payload;
    yield put(assignTruckRequested());
    yield call(() => myApi.assignTruckById(assignTruckId));
    yield put(assignTruckSuccess({ assignTruckId, userId }));
  } catch (error) {
    yield put(assignTruckError(error));
  }
}

export function* watchAssignTruck() {
  yield takeEvery(ASSIGN_TRUCK_START, assignTruckAsync);
}
