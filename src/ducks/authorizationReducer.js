import { call, put, takeEvery } from "redux-saga/effects";

import MyApi from "../api";

const myApi = new MyApi();

const TOGGLE_FORGOT_PASSWORD_MODAL =
  "nodejs_hw2_frontend/loads/TOGGLE_FORGOT_PASSWORD_MODAL";

const REGISTRATION_START =
  "nodejs_hw2_frontend/registration/REGISTRATION_START";
const REGISTRATION_REQUEST =
  "nodejs_hw2_frontend/registration/REGISTRATION_REQUEST";
const REGISTRATION_SUCCESS =
  "nodejs_hw2_frontend/registration/REGISTRATION_SUCCESS";
const REGISTRATION_FAILURE =
  "nodejs_hw2_frontend/registration/REGISTRATION_FAILURE";

const LOGIN_START = "nodejs_hw2_frontend/login/LOGIN_START";
const LOGIN_REQUEST = "nodejs_hw2_frontend/login/LOGIN_REQUEST";
const LOGIN_SUCCESS = "nodejs_hw2_frontend/login/LOGIN_SUCCESS";
const LOGIN_FAILURE = "nodejs_hw2_frontend/login/LOGIN_FAILURE";

const RESTORE_PASSWORD_START =
  "nodejs_hw2_frontend/login/RESTORE_PASSWORD_START";
const RESTORE_PASSWORD_REQUEST =
  "nodejs_hw2_frontend/login/RESTORE_PASSWORD_REQUEST";
const RESTORE_PASSWORD_SUCCESS =
  "nodejs_hw2_frontend/login/RESTORE_PASSWORD_SUCCESS";
const RESTORE_PASSWORD_FAILURE =
  "nodejs_hw2_frontend/login/RESTORE_PASSWORD_FAILURE";

const LOGOUT = "nodejs_hw2_frontend/login/LOGOUT";
const RELOGIN = "nodejs_hw2_frontend/login/RELOGIN";

const initialState = {
  loggedIn: false,
  loading: false,
  error: null,
  showForgotPasswordModal: false,
};

const authorizationReducer = (state = initialState, action) => {
  switch (action.type) {
    case TOGGLE_FORGOT_PASSWORD_MODAL:
      return {
        ...state,
        showForgotPasswordModal: !state.showForgotPasswordModal,
      };

    case REGISTRATION_REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case REGISTRATION_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
      };
    case REGISTRATION_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };

    case LOGIN_REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case LOGIN_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
        loggedIn: true,
      };
    case LOGIN_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };

    case RESTORE_PASSWORD_REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case RESTORE_PASSWORD_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
      };
    case RESTORE_PASSWORD_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };

    case LOGOUT:
      return {
        ...state,
        loggedIn: false,
      };

    case RELOGIN:
      return {
        ...state,
        loggedIn: true,
      };
    default:
      return state;
  }
};

export default authorizationReducer;

export const toggleForgotPasswordModal = () => ({
  type: TOGGLE_FORGOT_PASSWORD_MODAL,
});

const registrationRequested = () => ({
  type: REGISTRATION_REQUEST,
});
const registrationSuccess = () => ({
  type: REGISTRATION_SUCCESS,
});
const registrationError = (error) => ({
  type: REGISTRATION_FAILURE,
  payload: error,
});
export const registrationStart = (newUserData) => ({
  type: REGISTRATION_START,
  payload: newUserData,
});

const loginRequested = () => ({
  type: LOGIN_REQUEST,
});
const loginSuccess = () => ({
  type: LOGIN_SUCCESS,
});
const loginError = (error) => ({
  type: LOGIN_FAILURE,
  payload: error,
});
export const loginStart = (userData) => ({
  type: LOGIN_START,
  payload: userData,
});

const restorePasswordRequested = () => ({
  type: RESTORE_PASSWORD_REQUEST,
});
const restorePasswordSuccess = () => ({
  type: RESTORE_PASSWORD_SUCCESS,
});
const restorePasswordError = (error) => ({
  type: RESTORE_PASSWORD_FAILURE,
  payload: error,
});
export const restorePasswordStart = (email) => ({
  type: RESTORE_PASSWORD_START,
  payload: email,
});

export const relogin = () => ({
  type: RELOGIN,
});

export const logout = () => {
  localStorage.removeItem("jwt_token");
  return { type: LOGOUT };
};

function* registrationAsync(action) {
  try {
    const newUserData = action.payload;
    yield put(registrationRequested());
    yield call(() => myApi.register(newUserData));
    yield put(registrationSuccess());
  } catch (error) {
    yield put(registrationError(error));
  }
}

export function* watchRegistration() {
  yield takeEvery(REGISTRATION_START, registrationAsync);
}

function* LoginAsync(action) {
  try {
    const userData = action.payload;
    yield put(loginRequested());
    const result = yield call(() => myApi.login(userData));
    yield put(loginSuccess());
    localStorage.setItem("jwt_token", result.jwt_token);
  } catch (error) {
    yield put(loginError(error));
  }
}

export function* watchLogin() {
  yield takeEvery(LOGIN_START, LoginAsync);
}

function* restorePasswordAsync(action) {
  try {
    const email = action.payload;
    yield put(restorePasswordRequested());
    yield call(() => myApi.restorePassword(email));
    yield put(restorePasswordSuccess());
  } catch (error) {
    yield put(restorePasswordError(error));
  }
}

export function* watchRestorePassword() {
  yield takeEvery(RESTORE_PASSWORD_START, restorePasswordAsync);
}
