import { call, put, takeEvery } from "redux-saga/effects";

import MyApi from "../api";

const myApi = new MyApi();

const TOGGLE_LOADS_MODAL = "nodejs_hw2_frontend/loads/TOGGLE_LOADS_MODAL";

const SELECT_LOAD = "nodejs_hw2_frontend/loads/SELECT_LOAD";

const REMOVE_ALL_LOADS = "nodejs_hw2_frontend/loads/REMOVE_ALL_LOADS";

const REMOVE_ACTIVE_LOAD = "nodejs_hw2_frontend/loads/REMOVE_ACTIVE_LOAD";

const LOAD_LOADS_START = "nodejs_hw2_frontend/loads/LOAD_LOADS_START";
const LOAD_LOADS_REQUEST = "nodejs_hw2_frontend/loads/LOAD_LOADS_REQUEST";
const LOAD_LOADS_SUCCESS = "nodejs_hw2_frontend/loads/LOAD_LOADS_SUCCESS";
const LOAD_LOADS_FAILURE = "nodejs_hw2_frontend/loads/LOAD_LOADS_FAILURE";

const ADD_LOAD_START = "nodejs_hw2_frontend/loads/ADD_LOAD_START";
const ADD_LOAD_REQUEST = "nodejs_hw2_frontend/loads/ADD_LOAD_REQUEST";
const ADD_LOAD_SUCCESS = "nodejs_hw2_frontend/loads/ADD_LOAD_SUCCESS";
const ADD_LOAD_FAILURE = "nodejs_hw2_frontend/loads/ADD_LOAD_FAILURE";

const DELETE_LOAD_START = "nodejs_hw2_frontend/loads/DELETE_LOAD_START";
const DELETE_LOAD_REQUEST = "nodejs_hw2_frontend/loads/DELETE_LOAD_REQUEST";
const DELETE_LOAD_SUCCESS = "nodejs_hw2_frontend/loads/DELETE_LOAD_SUCCESS";
const DELETE_LOAD_FAILURE = "nodejs_hw2_frontend/loads/DELETE_LOAD_FAILURE";

const UPDATE_LOAD_START = "nodejs_hw2_frontend/loads/UPDATE_LOAD_START";
const UPDATE_LOAD_REQUEST = "nodejs_hw2_frontend/loads/UPDATE_LOAD_REQUEST";
const UPDATE_LOAD_SUCCESS = "nodejs_hw2_frontend/loads/UPDATE_LOAD_SUCCESS";
const UPDATE_LOAD_FAILURE = "nodejs_hw2_frontend/loads/UPDATE_LOAD_FAILURE";

const POST_LOAD_START = "nodejs_hw2_frontend/loads/POST_LOAD_START";
const POST_LOAD_REQUEST = "nodejs_hw2_frontend/loads/POST_LOAD_REQUEST";
const POST_LOAD_SUCCESS = "nodejs_hw2_frontend/loads/POST_LOAD_SUCCESS";
const POST_LOAD_FAILURE = "nodejs_hw2_frontend/loads/POST_LOAD_FAILURE";

const LOAD_ACTIVE_LOAD_START =
  "nodejs_hw2_frontend/loads/LOAD_ACTIVE_LOAD_START";
const LOAD_ACTIVE_LOAD_REQUEST =
  "nodejs_hw2_frontend/loads/LOAD_ACTIVE_LOAD_REQUEST";
const LOAD_ACTIVE_LOAD_SUCCESS =
  "nodejs_hw2_frontend/loads/LOAD_ACTIVE_LOAD_SUCCESS";
const LOAD_ACTIVE_LOAD_FAILURE =
  "nodejs_hw2_frontend/loads/LOAD_ACTIVE_LOAD_FAILURE";

const UPDATE_ACTIVE_LOAD_STATE_START =
  "nodejs_hw2_frontend/loads/UPDATE_ACTIVE_LOAD_STATE_START";
const UPDATE_ACTIVE_LOAD_STATE_REQUEST =
  "nodejs_hw2_frontend/loads/UPDATE_ACTIVE_LOAD_STATE_REQUEST";
const UPDATE_ACTIVE_LOAD_STATE_SUCCESS =
  "nodejs_hw2_frontend/loads/UPDATE_ACTIVE_LOAD_STATE_SUCCESS";
const UPDATE_ACTIVE_LOAD_STATE_FAILURE =
  "nodejs_hw2_frontend/loads/UPDATE_ACTIVE_LOAD_STATE_FAILURE";

const initialState = {
  activeLoad: null,
  selectedLoad: null,
  loads: [],
  loading: false,
  error: null,
  showLoadsModal: false,
};

const loadReducer = (state = initialState, action) => {
  switch (action.type) {
    case TOGGLE_LOADS_MODAL:
      return {
        ...state,
        showLoadsModal: !state.showLoadsModal,
      };
    case SELECT_LOAD:
      return {
        ...state,
        selectedLoad: action.payload,
      };
    case REMOVE_ALL_LOADS:
      return {
        ...state,
        loads: [],
      };
    case REMOVE_ACTIVE_LOAD:
      return {
        ...state,
        activeLoad: null,
      };

    case LOAD_LOADS_REQUEST:
      return {
        ...state,
        loads: [],
        loading: true,
        error: null,
      };
    case LOAD_LOADS_SUCCESS:
      return {
        ...state,
        loads: action.payload,
        loading: false,
        error: null,
      };
    case LOAD_LOADS_FAILURE:
      return {
        ...state,
        loads: [],
        loading: false,
        error: action.payload,
      };

    case ADD_LOAD_REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case ADD_LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
      };
    case ADD_LOAD_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };

    case DELETE_LOAD_REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case DELETE_LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
      };
    case DELETE_LOAD_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };

    case UPDATE_LOAD_REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case UPDATE_LOAD_SUCCESS:
      const load = action.payload;
      const loadIndex = state.loads.findIndex(({ _id }) => _id === load._id);
      return {
        ...state,
        loads: [
          ...state.loads.slice(0, loadIndex),
          load,
          ...state.loads.slice(loadIndex + 1),
        ],
        loading: false,
        error: null,
      };
    case UPDATE_LOAD_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };

    case POST_LOAD_REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case POST_LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
      };
    case POST_LOAD_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };

    case LOAD_ACTIVE_LOAD_REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case LOAD_ACTIVE_LOAD_SUCCESS:
      return {
        ...state,
        activeLoad: action.payload,
        loading: false,
        error: null,
      };
    case LOAD_ACTIVE_LOAD_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };

    case UPDATE_ACTIVE_LOAD_STATE_REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case UPDATE_ACTIVE_LOAD_STATE_SUCCESS:
      return {
        ...state,
        activeLoad: {
          ...state.activeLoad,
          state: action.payload,
        },
        loading: false,
        error: null,
      };
    case UPDATE_ACTIVE_LOAD_STATE_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    default:
      return state;
  }
};

export default loadReducer;

export const toggleLoadsModal = () => ({
  type: TOGGLE_LOADS_MODAL,
});

export const selectLoad = (load) => ({
  type: SELECT_LOAD,
  payload: load,
});

export const removeAllLoads = () => ({
  type: REMOVE_ALL_LOADS,
});

export const removeActiveLoad = () => ({
  type: REMOVE_ACTIVE_LOAD,
});

const loadsLoadSuccess = (newLoads) => ({
  type: LOAD_LOADS_SUCCESS,
  payload: newLoads,
});
const loadsLoadRequested = () => ({
  type: LOAD_LOADS_REQUEST,
});
const loadsLoadError = (error) => ({
  type: LOAD_LOADS_FAILURE,
  payload: error,
});
export const loadsLoadStart = (status) => ({
  type: LOAD_LOADS_START,
  payload: status,
});

const addLoadSuccess = () => ({
  type: ADD_LOAD_SUCCESS,
});
const addLoadRequested = () => ({
  type: ADD_LOAD_REQUEST,
});
const addLoadError = (error) => ({
  type: ADD_LOAD_FAILURE,
  payload: error,
});
export const addLoadStart = (newLoad) => ({
  type: ADD_LOAD_START,
  payload: newLoad,
});

const deleteLoadSuccess = (_id) => ({
  type: DELETE_LOAD_SUCCESS,
  payload: _id,
});
const deleteLoadRequested = () => ({
  type: DELETE_LOAD_REQUEST,
});
const deleteLoadError = (error) => ({
  type: DELETE_LOAD_FAILURE,
  payload: error,
});
export const deleteLoadStart = (_id) => ({
  type: DELETE_LOAD_START,
  payload: _id,
});

const updateLoadSuccess = (updatedLoad) => ({
  type: UPDATE_LOAD_SUCCESS,
  payload: updatedLoad,
});
const updateLoadRequested = () => ({
  type: UPDATE_LOAD_REQUEST,
});
const updateLoadError = (error) => ({
  type: UPDATE_LOAD_FAILURE,
  payload: error,
});
export const updateLoadStart = (updatedLoad) => ({
  type: UPDATE_LOAD_START,
  payload: updatedLoad,
});

const postLoadSuccess = () => ({
  type: POST_LOAD_SUCCESS,
});
const postLoadRequested = () => ({
  type: POST_LOAD_REQUEST,
});
const postLoadError = (error) => ({
  type: POST_LOAD_FAILURE,
  payload: error,
});
export const postLoadStart = (loadId) => ({
  type: POST_LOAD_START,
  payload: loadId,
});

const loadActiveLoadSuccess = (activeLoad) => ({
  type: LOAD_ACTIVE_LOAD_SUCCESS,
  payload: activeLoad,
});
const loadActiveLoadRequested = () => ({
  type: LOAD_ACTIVE_LOAD_REQUEST,
});
const loadActiveLoadError = (error) => ({
  type: LOAD_ACTIVE_LOAD_FAILURE,
  payload: error,
});
export const loadActiveLoadStart = () => ({
  type: LOAD_ACTIVE_LOAD_START,
});

const updateActiveLoadStateSuccess = (loadState) => ({
  type: UPDATE_ACTIVE_LOAD_STATE_SUCCESS,
  payload: loadState,
});
const updateActiveLoadStateRequested = () => ({
  type: UPDATE_ACTIVE_LOAD_STATE_REQUEST,
});
const updateActiveLoadStateError = (error) => ({
  type: UPDATE_ACTIVE_LOAD_STATE_FAILURE,
  payload: error,
});
export const updateActiveLoadStateStart = (newLoadState) => ({
  type: UPDATE_ACTIVE_LOAD_STATE_START,
  payload: newLoadState,
});

function* fetchLoadsAsync(action) {
  try {
    const status = action.payload;
    yield put(loadsLoadRequested());
    const data = yield call(() => myApi.getLoads(status));
    yield put(loadsLoadSuccess(data[0].loads));
  } catch (error) {
    yield put(loadsLoadError(error));
  }
}

export function* watchFetchLoads() {
  yield takeEvery(LOAD_LOADS_START, fetchLoadsAsync);
}

function* addLoadAsync(action) {
  try {
    const newLoad = action.payload;
    yield put(addLoadRequested());
    yield call(() => myApi.addLoad(newLoad));
    yield put(addLoadSuccess());
    yield put(loadsLoadStart("NEW"));
  } catch (error) {
    yield put(addLoadError(error));
  }
}

export function* watchAddLoad() {
  yield takeEvery(ADD_LOAD_START, addLoadAsync);
}

function* deleteLoadAsync(action) {
  try {
    const _id = action.payload;
    yield put(deleteLoadRequested());
    yield call(() => myApi.deleteLoadById(_id));
    yield put(deleteLoadSuccess());
    yield put(loadsLoadStart("NEW"));
  } catch (error) {
    yield put(deleteLoadError(error));
  }
}

export function* watchDeleteLoad() {
  yield takeEvery(DELETE_LOAD_START, deleteLoadAsync);
}

function* updateLoadAsync(action) {
  try {
    const updatedLoad = action.payload;
    yield put(updateLoadRequested());
    yield call(() => myApi.updateLoadById(updatedLoad));
    yield put(updateLoadSuccess(updatedLoad));
  } catch (error) {
    yield put(updateLoadError(error));
  }
}

export function* watchUpdateLoad() {
  yield takeEvery(UPDATE_LOAD_START, updateLoadAsync);
}

function* postLoadAsync(action) {
  try {
    const loadId = action.payload;
    yield put(postLoadRequested());
    yield call(() => myApi.postLoadById(loadId));
    yield put(postLoadSuccess());
    yield put(loadsLoadStart("NEW"));
  } catch (error) {
    yield put(postLoadError(error));
  }
}

export function* watchPostLoad() {
  yield takeEvery(POST_LOAD_START, postLoadAsync);
}

function* loadActiveLoadAsync(action) {
  try {
    yield put(loadActiveLoadRequested());
    const { activeDriverLoad } = yield call(() => myApi.getActiveLoad());
    yield put(loadActiveLoadSuccess(activeDriverLoad));
  } catch (error) {
    yield put(loadActiveLoadError(error));
  }
}

export function* watchloadActiveLoad() {
  yield takeEvery(LOAD_ACTIVE_LOAD_START, loadActiveLoadAsync);
}

function* updateActiveLoadStateAsync(action) {
  try {
    const newState = action.payload;
    yield put(updateActiveLoadStateRequested());
    yield call(() => myApi.iterateToNextActiveLoadState());
    yield put(updateActiveLoadStateSuccess(newState));
    if (newState === "Arrived to delivery") {
      yield put(removeActiveLoad());
    }
  } catch (error) {
    yield put(updateActiveLoadStateError(error));
  }
}

export function* watchUpdateActiveLoadState() {
  yield takeEvery(UPDATE_ACTIVE_LOAD_STATE_START, updateActiveLoadStateAsync);
}
