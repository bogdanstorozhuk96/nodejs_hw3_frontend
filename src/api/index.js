export default class MyApi {
  clientId = "3e253d7b5654c43";
  clientSecret = "a00fa144397aff2f1597d1fd44d2206ff2ad8f95";
  _apiBase = "http://localhost:8080/api";
  _imgurApiBase = "https://api.imgur.com/3";

  handleErrors = (response) => {
    if (!response.ok) {
      throw Error(response.statusText);
    }
    return response;
  };

  uploadToImgur = async (image) => {
    try {
      const result = await fetch(`${this._imgurApiBase}/image`, {
        method: "POST",
        mode: "no-cors",
        body: image.toString("base64"),
        headers: {
          Authorization: `Client-ID ${this.clientId}`,
        },
      });
      this.handleErrors(result);
    } catch (error) {
      throw error;
    }
  };

  register = async (newUserData) => {
    const { email, password, role } = newUserData;
    try {
      const result = await fetch(`${this._apiBase}/auth/register`, {
        method: "POST",
        mode: "cors",
        body: JSON.stringify({
          email,
          password,
          role,
        }),
        headers: {
          "Content-type": "application/json; charset=UTF-8",
        },
      });
      this.handleErrors(result);
    } catch (error) {
      throw error;
    }
  };

  login = async (userData) => {
    const { email, password } = userData;
    try {
      const result = await fetch(`${this._apiBase}/auth/login`, {
        method: "POST",
        mode: "cors",
        body: JSON.stringify({
          email,
          password,
        }),
        headers: {
          "Content-type": "application/json; charset=UTF-8",
        },
      });
      return await this.handleErrors(result).json();
    } catch (error) {
      throw error;
    }
  };

  restorePassword = async (email) => {
    try {
      const result = await fetch(`${this._apiBase}/auth/forgot_password`, {
        method: "POST",
        mode: "cors",
        body: JSON.stringify({
          email,
        }),
        headers: {
          "Content-type": "application/json; charset=UTF-8",
        },
      });
      return await this.handleErrors(result).json();
    } catch (error) {
      throw error;
    }
  };

  getUser = async () => {
    try {
      const result = await fetch(`${this._apiBase}/users/me`, {
        method: "GET",
        mode: "cors",
        headers: {
          "Content-type": "application/json; charset=UTF-8",
          Authorization: `Bearer ${localStorage.getItem("jwt_token")}`,
        },
      });
      return await this.handleErrors(result).json();
    } catch (error) {
      throw error;
    }
  };

  changePassword = async (newPassword, oldPassword) => {
    try {
      const result = await fetch(`${this._apiBase}/users/me/password`, {
        method: "PATCH",
        mode: "cors",
        body: JSON.stringify({
          oldPassword,
          newPassword,
        }),
        headers: {
          "Content-type": "application/json; charset=UTF-8",
          Authorization: `Bearer ${localStorage.getItem("jwt_token")}`,
        },
      });
      return await this.handleErrors(result).json();
    } catch (error) {
      throw error;
    }
  };

  deleteUser = async () => {
    try {
      const result = await fetch(`${this._apiBase}/users/me`, {
        method: "DELETE",
        mode: "cors",
        headers: {
          "Content-type": "application/json; charset=UTF-8",
          Authorization: `Bearer ${localStorage.getItem("jwt_token")}`,
        },
      });
      return await this.handleErrors(result).json();
    } catch (error) {
      throw error;
    }
  };

  getLoads = async (status, offset) => {
    try {
      if (offset) {
        offset = `&offset=${offset}`;
      }
      const url = offset
        ? `${this._apiBase}/loads?status=${status}${offset}&limit=10`
        : `${this._apiBase}/loads?status=${status}&limit=10`;
      const result = await fetch(url, {
        method: "GET",
        mode: "cors",
        headers: {
          "Content-type": "application/json; charset=UTF-8",
          Authorization: `Bearer ${localStorage.getItem("jwt_token")}`,
        },
      });
      return await this.handleErrors(result).json();
    } catch (error) {
      throw error;
    }
  };

  addLoad = async (newLoad) => {
    try {
      const result = await fetch(`${this._apiBase}/loads`, {
        method: "POST",
        mode: "cors",
        body: JSON.stringify(newLoad),
        headers: {
          "Content-type": "application/json; charset=UTF-8",
          Authorization: `Bearer ${localStorage.getItem("jwt_token")}`,
        },
      });
      return await this.handleErrors(result).json();
    } catch (error) {
      throw error;
    }
  };

  deleteLoadById = async (_id) => {
    try {
      const result = await fetch(`${this._apiBase}/loads/${_id}`, {
        method: "DELETE",
        mode: "cors",
        headers: {
          "Content-type": "application/json; charset=UTF-8",
          Authorization: `Bearer ${localStorage.getItem("jwt_token")}`,
        },
      });
      return await this.handleErrors(result).json();
    } catch (error) {
      throw error;
    }
  };

  updateLoadById = async ({
    name,
    pickup_address,
    delivery_address,
    payload,
    dimensions,
    _id,
  }) => {
    try {
      const result = await fetch(`${this._apiBase}/loads/${_id}`, {
        method: "PUT",
        mode: "cors",
        body: JSON.stringify({
          name,
          pickup_address,
          delivery_address,
          payload,
          dimensions,
        }),
        headers: {
          "Content-type": "application/json; charset=UTF-8",
          Authorization: `Bearer ${localStorage.getItem("jwt_token")}`,
        },
      });
      return await this.handleErrors(result).json();
    } catch (error) {
      throw error;
    }
  };

  getTrucks = async () => {
    try {
      const result = await fetch(`${this._apiBase}/trucks`, {
        method: "GET",
        mode: "cors",
        headers: {
          "Content-type": "application/json; charset=UTF-8",
          Authorization: `Bearer ${localStorage.getItem("jwt_token")}`,
        },
      });
      return await this.handleErrors(result).json();
    } catch (error) {
      throw error;
    }
  };

  addTruck = async (newTruck) => {
    try {
      const result = await fetch(`${this._apiBase}/trucks`, {
        method: "POST",
        mode: "cors",
        body: JSON.stringify(newTruck),
        headers: {
          "Content-type": "application/json; charset=UTF-8",
          Authorization: `Bearer ${localStorage.getItem("jwt_token")}`,
        },
      });
      return await this.handleErrors(result).json();
    } catch (error) {
      throw error;
    }
  };

  deleteTruckById = async (_id) => {
    try {
      const result = await fetch(`${this._apiBase}/trucks/${_id}`, {
        method: "DELETE",
        mode: "cors",
        headers: {
          "Content-type": "application/json; charset=UTF-8",
          Authorization: `Bearer ${localStorage.getItem("jwt_token")}`,
        },
      });
      return await this.handleErrors(result).json();
    } catch (error) {
      throw error;
    }
  };

  updateTruckById = async ({ type, _id }) => {
    try {
      const result = await fetch(`${this._apiBase}/trucks/${_id}`, {
        method: "PUT",
        mode: "cors",
        body: JSON.stringify({
          type,
        }),
        headers: {
          "Content-type": "application/json; charset=UTF-8",
          Authorization: `Bearer ${localStorage.getItem("jwt_token")}`,
        },
      });
      return await this.handleErrors(result).json();
    } catch (error) {
      throw error;
    }
  };

  assignTruckById = async (assignTruckId) => {
    try {
      const result = await fetch(
        `${this._apiBase}/trucks/${assignTruckId}/assign`,
        {
          method: "POST",
          mode: "cors",
          headers: {
            "Content-type": "application/json; charset=UTF-8",
            Authorization: `Bearer ${localStorage.getItem("jwt_token")}`,
          },
        }
      );
      return await this.handleErrors(result).json();
    } catch (error) {
      throw error;
    }
  };

  postLoadById = async (loadId) => {
    try {
      const result = await fetch(`${this._apiBase}/loads/${loadId}/post`, {
        method: "POST",
        mode: "cors",
        headers: {
          "Content-type": "application/json; charset=UTF-8",
          Authorization: `Bearer ${localStorage.getItem("jwt_token")}`,
        },
      });
      return await this.handleErrors(result).json();
    } catch (error) {
      throw error;
    }
  };

  getActiveLoad = async () => {
    try {
      const result = await fetch(`${this._apiBase}/loads/active`, {
        method: "GET",
        mode: "cors",
        headers: {
          "Content-type": "application/json; charset=UTF-8",
          Authorization: `Bearer ${localStorage.getItem("jwt_token")}`,
        },
      });
      return await this.handleErrors(result).json();
    } catch (error) {
      throw error;
    }
  };

  iterateToNextActiveLoadState = async () => {
    try {
      const result = await fetch(`${this._apiBase}/loads/active/state`, {
        method: "PATCH",
        mode: "cors",
        headers: {
          "Content-type": "application/json; charset=UTF-8",
          Authorization: `Bearer ${localStorage.getItem("jwt_token")}`,
        },
      });
      return await this.handleErrors(result).json();
    } catch (error) {
      throw error;
    }
  };
}
