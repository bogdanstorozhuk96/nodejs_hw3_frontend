import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { Container, Button } from "react-bootstrap";

import { Spinner, ErrorIndicator, Truck } from ".";
import { toggleTrucksModal } from "../ducks";

const TruckList = ({ trucks, toggle }) => {
  return (
    <Container>
      <table className="table">
        <thead>
          <tr>
            <th scope="col">TYPE</th>
            <th scope="col">CREATED DATE</th>
            <th>
              <Button onClick={toggle}>Add new truck</Button>
            </th>
          </tr>
        </thead>
        <tbody>
          {trucks.map((truck) => (
            <Truck truck={truck} key={truck._id} />
          ))}
        </tbody>
      </table>
    </Container>
  );
};

const TruckListContainer = () => {
  const dispatch = useDispatch();

  const trucks = useSelector((state) => state.truckReducer.trucks);
  const loading = useSelector((state) => state.truckReducer.loading);
  const error = useSelector((state) => state.truckReducer.error);

  if (loading) {
    return <Spinner />;
  }

  if (error) {
    return <ErrorIndicator />;
  }

  return (
    <TruckList trucks={trucks} toggle={() => dispatch(toggleTrucksModal())} />
  );
};

export default TruckListContainer;
