import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { Container, Button } from "react-bootstrap";

import { Spinner, ErrorIndicator } from ".";
import { updateActiveLoadStateStart } from "../ducks";
import { formatDuration, getNextLoadState } from "../utils";
import { StyledNoDataMessage } from "../styled";

const ActiveLoad = ({ activeLoad }) => {
  const dispatch = useDispatch();

  const postDate = new Date(activeLoad.created_date);
  const currentDate = new Date();
  const seconds = (currentDate.getTime() - postDate.getTime()) / 1000;

  const nextLoadState = getNextLoadState(activeLoad.state);

  return (
    <Container>
      <table className="table">
        <thead>
          <tr>
            <th scope="col">LOAD NAME</th>
            <th scope="col">CREATED DATE</th>
            <th scope="col">PICK UP ADDRESS</th>
            <th scope="col">DELIVERY ADDRESS</th>
            <th scope="col">STATE</th>
            <th scope="col">UPDATE STATE</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>{activeLoad.name}</td>
            <td>{formatDuration(seconds)} ago</td>
            <td>{activeLoad.pickup_address}</td>
            <td>{activeLoad.delivery_address}</td>
            <td>{activeLoad.state}</td>
            <td>
              <Button
                onClick={() => {
                  dispatch(updateActiveLoadStateStart(nextLoadState));
                }}
                className="mr-2 mb-2"
              >
                {nextLoadState}
              </Button>
            </td>
          </tr>
        </tbody>
      </table>
    </Container>
  );
};

const ActiveLoadPageContainer = () => {
  const activeLoad = useSelector((state) => state.loadReducer.activeLoad);
  const loading = useSelector((state) => state.loadReducer.loading);
  const error = useSelector((state) => state.loadReducer.error);

  if (loading) {
    return <Spinner />;
  }

  if (error) {
    return <ErrorIndicator />;
  }

  if (!activeLoad) {
    return <StyledNoDataMessage>No active payload</StyledNoDataMessage>;
  }

  return <ActiveLoad activeLoad={activeLoad} />;
};

export default ActiveLoadPageContainer;
