import React, { Fragment } from "react";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

import { StyledMenu } from "../styled";

const Menu = ({ open, ...props }) => {
  const user = useSelector((state) => state.userReducer.user);

  const isHidden = open ? true : false;
  const tabIndex = isHidden ? 0 : -1;

  return (
    <StyledMenu open={open} aria-hidden={!isHidden} {...props}>
      <Link to={"/"} tabIndex={tabIndex}>
        {user.role === "SHIPPER" ? "New loads" : "Trucks in service"}
      </Link>
      {user.role === "SHIPPER" ? (
        <Fragment>
          <Link to={"/posted_loads"} tabIndex={tabIndex}>
            Posted loads
          </Link>
          <Link to={"/assigned_loads"} tabIndex={tabIndex}>
            Assigend loads
          </Link>
          <Link to={"/history"} tabIndex={tabIndex}>
            History
          </Link>
        </Fragment>
      ) : (
        <Fragment>
          <Link to={"/active_load"} tabIndex={tabIndex}>
            Interact with load
          </Link>
          <Link to={"/history"} tabIndex={tabIndex}>
            History
          </Link>
        </Fragment>
      )}
      <Link to={"/profile"} tabIndex={tabIndex}>
        Profile
      </Link>
    </StyledMenu>
  );
};

export default Menu;
