import React, { Fragment } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button } from "react-bootstrap";

import { formatDuration } from "../utils";
import {
  deleteTruckStart,
  toggleTrucksModal,
  selectTruck,
  assignTruckStart,
} from "../ducks";

const Truck = ({ truck }) => {
  const user = useSelector((state) => state.userReducer.user);
  const activeLoad = useSelector((state) => state.loadReducer.activeLoad);

  const dispatch = useDispatch();

  const onDelete = () => {
    dispatch(deleteTruckStart(truck._id));
  };

  const onAssign = () => {
    dispatch(assignTruckStart(truck._id, user._id));
  };
  const toggleEditModal = () => {
    dispatch(selectTruck({ ...truck }));
    dispatch(toggleTrucksModal());
  };

  const postDate = new Date(truck.created_date);
  const currentDate = new Date();
  const seconds = (currentDate.getTime() - postDate.getTime()) / 1000;

  let element = null;
  if (!truck.assigned_to && !activeLoad) {
    element = (
      <Button onClick={onAssign} className="mr-2 mb-2">
        Assign
      </Button>
    );
  } else if (truck.assigned_to) {
    element = <span>Assigned</span>;
  }

  return (
    <tr>
      <td>{truck.type}</td>
      <td>{formatDuration(seconds)} ago</td>
      <td>
        {!activeLoad ? (
          <Fragment>
            <Button onClick={onDelete} className="mr-2 mb-2">
              Delete
            </Button>
            <Button onClick={toggleEditModal} className="mr-2 mb-2">
              Edit
            </Button>
          </Fragment>
        ) : null}
        {element}
      </td>
    </tr>
  );
};

export default Truck;
