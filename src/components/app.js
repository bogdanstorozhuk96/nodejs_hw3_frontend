import React, { Fragment, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Route, Switch } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";

import {
  LoadPage,
  Header,
  ProfilePage,
  TruckPage,
  ActiveLoadPageContainer,
} from ".";
import {
  relogin,
  loadUserStart,
  trucksLoadStart,
  loadActiveLoadStart,
} from "../ducks/";

const App = () => {
  const dispatch = useDispatch();

  const loggedIn = useSelector((state) => state.authorizationReducer.loggedIn);
  const user = useSelector((state) => state.userReducer.user);

  useEffect(() => {
    if (localStorage.getItem("jwt_token")) {
      dispatch(relogin());
    }
    if (loggedIn) {
      if (user.role === "DRIVER") {
        dispatch(trucksLoadStart());
        dispatch(loadActiveLoadStart());
      }
      dispatch(loadUserStart());
    }
  }, [dispatch, loggedIn, user.role]);

  return (
    <Fragment>
      <Header />
      <Switch>
        <Route
          path="/"
          render={({ match }) => {
            const { url } = match;
            if (user.role === "SHIPPER") {
              return <LoadPage url={url} />;
            } else if (user.role === "DRIVER") {
              return <TruckPage />;
            }
          }}
          exact
        />
        <Route
          path="/posted_loads/"
          render={({ match }) => {
            const { url } = match;
            return <LoadPage url={url} />;
          }}
        />
        <Route
          path="/assigned_loads/"
          render={({ match }) => {
            const { url } = match;
            return <LoadPage url={url} />;
          }}
        />
        <Route
          path="/history/"
          render={({ match }) => {
            const { url } = match;
            if (user.role === "SHIPPER") {
              return <LoadPage url={url} />;
            } else if (user.role === "DRIVER") {
              return <LoadPage url={url} />;
            }
          }}
        />
        <Route path="/active_load/" component={ActiveLoadPageContainer} />
        <Route path="/profile/" component={ProfilePage} />
      </Switch>
    </Fragment>
  );
};

export default App;
