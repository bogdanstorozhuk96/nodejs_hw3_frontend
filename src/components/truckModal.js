import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Modal, DropdownButton, Dropdown } from "react-bootstrap";

import { ErrorIndicator, Spinner } from ".";
import {
  toggleTrucksModal,
  addTruckStart,
  selectTruck,
  updateTruckStart,
} from "../ducks";

const TruckModal = () => {
  const dispatch = useDispatch();

  const loading = useSelector((state) => state.truckReducer.loading);
  const error = useSelector((state) => state.truckReducer.error);
  const selectedTruck = useSelector(
    (state) => state.truckReducer.selectedTruck
  );
  const showTrucksModal = useSelector(
    (state) => state.truckReducer.showTrucksModal
  );

  const [type, setType] = useState(selectedTruck ? selectedTruck.type : "");

  const closeModal = () => {
    dispatch(toggleTrucksModal());
    if (selectedTruck) {
      dispatch(selectTruck(null));
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    if (selectedTruck) {
      dispatch(
        updateTruckStart({
          _id: selectedTruck._id,
          type,
        })
      );
    } else {
      dispatch(
        addTruckStart({
          type,
        })
      );
    }
  };

  let element;
  if (loading) {
    element = <Spinner />;
  } else if (error) {
    element = <ErrorIndicator />;
  } else {
    element = null;
  }

  return (
    <Modal size="md" show={showTrucksModal} onHide={closeModal}>
      <Modal.Header closeButton>
        <Modal.Title>
          {selectedTruck ? "Edit truck" : "Add new truck"}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <form onSubmit={handleSubmit}>
          <DropdownButton
            id="dropdown-basic-button"
            title="Dropdown button"
            className="mb-2"
          >
            <Dropdown.Item onSelect={() => setType("SPRINTER")}>
              SPRINTER
            </Dropdown.Item>
            <Dropdown.Item onSelect={() => setType("SMALL STRAIGHT")}>
              SMALL STRAIGHT
            </Dropdown.Item>
            <Dropdown.Item onSelect={() => setType("LARGE STRAIGHT")}>
              LARGE STRAIGHT
            </Dropdown.Item>
          </DropdownButton>
          <button type="submit" className="btn btn-primary">
            Submit
          </button>
        </form>
        {element}
      </Modal.Body>
    </Modal>
  );
};

export default TruckModal;
