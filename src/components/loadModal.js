import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Modal } from "react-bootstrap";

import { ErrorIndicator, Spinner, FormItem } from ".";
import {
  toggleLoadsModal,
  addLoadStart,
  selectLoad,
  updateLoadStart,
} from "../ducks";

const LoadModal = () => {
  const dispatch = useDispatch();

  const loading = useSelector((state) => state.loadReducer.loading);
  const error = useSelector((state) => state.loadReducer.error);
  const selectedLoad = useSelector((state) => state.loadReducer.selectedLoad);
  const showLoadsModal = useSelector(
    (state) => state.loadReducer.showLoadsModal
  );

  const [loadName, setLoadName] = useState(
    selectedLoad ? selectedLoad.name : ""
  );
  const [pickUpAddress, setPickUpAddress] = useState(
    selectedLoad ? selectedLoad.pickup_address : ""
  );
  const [deliveryAddress, setDeliveryAddress] = useState(
    selectedLoad ? selectedLoad.delivery_address : ""
  );
  const [payload, setPayload] = useState(
    selectedLoad ? selectedLoad.payload : ""
  );
  const [width, setWidth] = useState(
    selectedLoad ? selectedLoad.dimensions.width : ""
  );
  const [length, setLength] = useState(
    selectedLoad ? selectedLoad.dimensions.length : ""
  );
  const [height, setHeight] = useState(
    selectedLoad ? selectedLoad.dimensions.height : ""
  );

  const closeModal = () => {
    dispatch(toggleLoadsModal());
    if (selectedLoad) {
      dispatch(selectLoad(null));
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    if (selectedLoad) {
      dispatch(
        updateLoadStart({
          _id: selectedLoad._id,
          name: loadName,
          pickup_address: pickUpAddress,
          delivery_address: deliveryAddress,
          payload,
          dimensions: { width, length, height },
        })
      );
    } else {
      dispatch(
        addLoadStart({
          name: loadName,
          pickup_address: pickUpAddress,
          delivery_address: deliveryAddress,
          payload,
          dimensions: { width, length, height },
        })
      );
    }
  };

  let element;
  if (loading) {
    element = <Spinner />;
  } else if (error) {
    element = <ErrorIndicator />;
  } else {
    element = null;
  }

  return (
    <Modal size="md" show={showLoadsModal} onHide={closeModal}>
      <Modal.Header closeButton>
        <Modal.Title>{selectedLoad ? "Edit load" : "Add new load"}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <form onSubmit={handleSubmit}>
          <FormItem
            labelText={"name"}
            elementId={"name"}
            property={loadName}
            setProperty={setLoadName}
            classes="form-group"
          />
          <FormItem
            labelText={"Pick up address"}
            elementId={"pickup_address"}
            property={pickUpAddress}
            setProperty={setPickUpAddress}
            classes="form-group"
          />
          <FormItem
            labelText={"Delivery address"}
            elementId={"delivery_address"}
            property={deliveryAddress}
            setProperty={setDeliveryAddress}
            classes="form-group"
          />
          <FormItem
            labelText={"Payload"}
            elementId={"payload"}
            property={payload}
            setProperty={setPayload}
            classes="form-group"
          />
          <div className="form-row">
            <FormItem
              labelText={"Width"}
              elementId={"width"}
              property={width}
              setProperty={setWidth}
              classes="form-group col-md-4"
            />
            <FormItem
              labelText={"Length"}
              elementId={"length"}
              property={length}
              setProperty={setLength}
              classes="form-group col-md-4"
            />
            <FormItem
              labelText={"Height"}
              elementId={"height"}
              property={height}
              setProperty={setHeight}
              classes="form-group col-md-4"
            />
          </div>
          <button type="submit" className="btn btn-primary">
            Submit
          </button>
        </form>
        {element}
      </Modal.Body>
    </Modal>
  );
};

export default LoadModal;
