import React, { useState, Fragment } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Modal, Button } from "react-bootstrap";

import {
  registrationStart,
  loginStart,
  toggleForgotPasswordModal,
} from "../ducks";
import { ErrorIndicator, Spinner } from ".";
import { validateEmail } from "../utils";

const AuthModal = ({
  openModal,
  show,
  setModalForRegistration,
  isRegister,
}) => {
  const dispatch = useDispatch();

  const loading = useSelector((state) => state.authorizationReducer.loading);
  const error = useSelector((state) => state.authorizationReducer.error);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [userRole, setUserRole] = useState(false);
  const [IsError, setError] = useState(false);

  const handleSubmit = (event) => {
    event.preventDefault();
    if (!password || !validateEmail(email)) {
      setError(true);
    } else {
      if (isRegister) {
        const role = userRole ? "DRIVER" : "SHIPPER";
        dispatch(registrationStart({ email, password, role }));
      } else {
        dispatch(loginStart({ email, password }));
      }
      setError(false);
    }
  };

  const displayForgotPasswordModal = () => {
    openModal();
    dispatch(toggleForgotPasswordModal());
  };

  const changePassword = (event) => {
    setPassword(event.target.value);
  };

  const changeEmail = (event) => {
    setEmail(event.target.value);
  };

  let element;
  if (loading) {
    element = <Spinner />;
  } else if (error) {
    element = <ErrorIndicator />;
  } else {
    element = null;
  }
  return (
    <Modal
      size="sm"
      show={show}
      onHide={() => openModal()}
      aria-labelledby="example-modal-sizes-title-sm"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-sm">
          {isRegister ? "Register" : "Login"}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <form onSubmit={handleSubmit}>
          <div className="form-group">
            <label htmlFor="email">Email</label>
            <input
              type="text"
              className="form-control"
              value={email}
              onChange={changeEmail}
              id="email"
              placeholder="email"
            />
          </div>
          <div className="form-group">
            <label htmlFor="password">Password</label>
            <input
              type="password"
              className="form-control"
              value={password}
              id="password"
              placeholder="password"
              onChange={changePassword}
            />
          </div>
          {isRegister ? (
            <Fragment>
              <div className="form-check">
                <input
                  type="checkbox"
                  className="form-check-input"
                  id="userRole"
                  onClick={() => setUserRole(!userRole)}
                />
                <label className="form-check-label" htmlFor="userRole">
                  Driver
                </label>
              </div>
            </Fragment>
          ) : null}
          {IsError ? (
            <p className="text-danger">Provided data has wrong format</p>
          ) : null}
          <button type="submit" className="btn btn-primary">
            Submit
          </button>
        </form>
        {element}
      </Modal.Body>
      {isRegister ? null : (
        <Modal.Footer>
          <Button variant="primary" onClick={displayForgotPasswordModal}>
            Forgot password
          </Button>
          <Button variant="primary" onClick={setModalForRegistration}>
            Register
          </Button>
        </Modal.Footer>
      )}
    </Modal>
  );
};

export default AuthModal;
