import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Container, Button } from "react-bootstrap";

import { Spinner, ErrorIndicator, Load } from ".";
import { toggleLoadsModal, loadsLoadStart } from "../ducks";
import { StyledNoDataMessage } from "../styled";

const LoadList = ({ loads, toggle, url }) => {
  return (
    <Container>
      <table className="table">
        <thead>
          <tr>
            <th scope="col">LOAD NAME</th>
            <th scope="col">CREATED DATE</th>
            <th scope="col">PICK UP ADDRESS</th>
            <th scope="col">DELIVERY ADDRESS</th>
            {url === "/" ? (
              <th>
                <Button onClick={toggle}>Add new load</Button>
              </th>
            ) : null}
          </tr>
        </thead>
        <tbody>
          {loads.length !== 0 ? (
            loads.map((load) => <Load load={load} key={load._id} url={url} />)
          ) : (
            <tr>
              <td>
                <StyledNoDataMessage>No data to display</StyledNoDataMessage>
              </td>
            </tr>
          )}
        </tbody>
      </table>
    </Container>
  );
};

const LoadListContainer = ({ url }) => {
  const dispatch = useDispatch();

  const loggedIn = useSelector((state) => state.authorizationReducer.loggedIn);
  const loads = useSelector((state) => state.loadReducer.loads);
  const loading = useSelector((state) => state.loadReducer.loading);
  const error = useSelector((state) => state.loadReducer.error);

  useEffect(() => {
    if (loggedIn) {
      if (url === "/") {
        dispatch(loadsLoadStart("NEW"));
      } else if (url === "/assigned_loads") {
        dispatch(loadsLoadStart("ASSIGNED"));
      } else if (url === "/posted_loads") {
        dispatch(loadsLoadStart("POSTED"));
      } else if (url === "/history") {
        dispatch(loadsLoadStart("SHIPPED"));
      }
    }
  }, [url, dispatch, loggedIn]);

  if (loading) {
    return <Spinner />;
  }

  if (error) {
    return <ErrorIndicator />;
  }

  return (
    <LoadList
      loads={loads}
      toggle={() => dispatch(toggleLoadsModal())}
      url={url}
    />
  );
};

export default LoadListContainer;
