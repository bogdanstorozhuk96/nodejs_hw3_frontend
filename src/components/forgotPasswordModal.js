import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Modal, Button, Form } from "react-bootstrap";

import { toggleForgotPasswordModal, restorePasswordStart } from "../ducks";

import { ErrorIndicator, Spinner, FormItem } from ".";

const ForgotPasswordModal = () => {
  const dispatch = useDispatch();

  const loading = useSelector((state) => state.authorizationReducer.loading);
  const error = useSelector((state) => state.authorizationReducer.error);
  const showForgotPasswordModal = useSelector(
    (state) => state.authorizationReducer.showForgotPasswordModal
  );

  const [email, setEmail] = useState("");

  const handleSubmit = (event) => {
    event.preventDefault();
    dispatch(restorePasswordStart(email));
  };

  const closeModal = () => {
    dispatch(toggleForgotPasswordModal());
  };

  let element;
  if (loading) {
    element = <Spinner />;
  } else if (error) {
    element = <ErrorIndicator />;
  } else {
    element = null;
  }

  return (
    <Modal
      size="sm"
      show={showForgotPasswordModal}
      onHide={closeModal}
      aria-labelledby="example-modal-sizes-title-sm"
    >
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-sm">
          Lost password
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <p>
          If you have forgotten your password, you can use this form to reset
          your password. You will receive an email with instructions.
        </p>
      </Modal.Body>
      <Modal.Footer>
        <Form onSubmit={handleSubmit}>
          <FormItem
            labelText={"Email"}
            elementId={"email"}
            property={email}
            setProperty={setEmail}
            classes={"form-group"}
          />
          <Button type="submit">Submit</Button>
        </Form>
        {element}
      </Modal.Footer>
    </Modal>
  );
};

export default ForgotPasswordModal;
