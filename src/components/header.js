import React, { useState, Fragment } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { Button } from "react-bootstrap";

import { Burger, Menu, AuthModal, ForgotPasswordModal } from ".";
import {
  StyledNavbarNav,
  StyledHeaderMessage,
  StyledButtonContainer,
} from "../styled";
import { logout, removeAllLoads, removeAllTrucks } from "../ducks";

const Header = () => {
  const dispatch = useDispatch();
  const history = useHistory();

  const loggedIn = useSelector((state) => state.authorizationReducer.loggedIn);
  const user = useSelector((state) => state.userReducer.user);
  const showForgotPasswordModal = useSelector(
    (state) => state.authorizationReducer.showForgotPasswordModal
  );

  const [open, setOpen] = useState(false);
  const [show, setShow] = useState(false);
  const [isRegister, setIsRegister] = useState(false);

  const openModal = () => {
    setShow(!show);
    setIsRegister(false);
  };

  const setModalForRegistration = () => {
    setIsRegister(true);
  };

  const logoutFromAccount = () => {
    dispatch(logout());
    if (user.role === "SHIPPER") {
      dispatch(removeAllLoads());
    } else if (user.role === "DRIVER") {
      dispatch(removeAllTrucks());
    }
    history.push("/");
  };

  return (
    <Fragment>
      <nav className="navbar navbar-expand-sm navbar-light bg-light">
        <StyledNavbarNav className="navbar-nav">
          {loggedIn ? (
            <Fragment>
              <Burger open={open} setOpen={setOpen} />
              <StyledHeaderMessage>
                Welcome,{" "}
                {user.email
                  ? user.email.substring(0, user.email.indexOf("@"))
                  : null}
              </StyledHeaderMessage>
            </Fragment>
          ) : null}
          <StyledButtonContainer>
            {loggedIn ? (
              <Button
                onClick={logoutFromAccount}
                className="btn-secondary nav-item"
              >
                Logout
              </Button>
            ) : (
              <Button onClick={openModal} className="nav-item">
                Login
              </Button>
            )}
          </StyledButtonContainer>
        </StyledNavbarNav>
      </nav>
      <Menu open={open} setOpen={setOpen} />
      {show ? (
        <AuthModal
          openModal={openModal}
          setModalForRegistration={setModalForRegistration}
          show={show}
          isRegister={isRegister}
        />
      ) : null}
      {showForgotPasswordModal ? <ForgotPasswordModal /> : null}
    </Fragment>
  );
};

export default Header;
