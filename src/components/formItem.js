import React from "react";

const FormItem = ({ labelText, elementId, property, setProperty, classes }) => {
  return (
    <div className={classes}>
      <label htmlFor={elementId}>{labelText}</label>
      <input
        type="text"
        className="form-control"
        id={elementId}
        value={property}
        onChange={(event) => setProperty(event.target.value)}
        required
      />
    </div>
  );
};

export default FormItem;
