import React, { Fragment } from "react";
import { useSelector } from "react-redux";

import { LoadListContainer, LoadModal } from ".";

const LoadPage = ({ url }) => {
  const showLoadsModal = useSelector(
    (state) => state.loadReducer.showLoadsModal
  );
  return (
    <Fragment>
      <LoadListContainer url={url} />
      {showLoadsModal && url === "/" ? <LoadModal /> : null}
    </Fragment>
  );
};

export default LoadPage;
