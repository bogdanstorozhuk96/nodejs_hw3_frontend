import React, { Fragment } from "react";
import { useSelector } from "react-redux";

import { TruckListContainer, TruckModal } from ".";

const TruckPage = () => {
  const showTrucksModal = useSelector(
    (state) => state.truckReducer.showTrucksModal
  );
  return (
    <Fragment>
      <TruckListContainer />
      {showTrucksModal ? <TruckModal /> : null}
    </Fragment>
  );
};

export default TruckPage;
