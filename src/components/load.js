import React from "react";
import { useDispatch } from "react-redux";
import { Button } from "react-bootstrap";

import { formatDuration } from "../utils";
import {
  deleteLoadStart,
  toggleLoadsModal,
  selectLoad,
  postLoadStart,
} from "../ducks";

const Load = ({ load, url }) => {
  const dispatch = useDispatch();

  const onDelete = () => {
    dispatch(deleteLoadStart(load._id));
  };

  const toggleEditModal = () => {
    dispatch(selectLoad({ ...load }));
    dispatch(toggleLoadsModal());
  };

  const onPost = () => {
    dispatch(postLoadStart(load._id));
  };

  const postDate = new Date(load.created_date);
  const currentDate = new Date();
  const seconds = (currentDate.getTime() - postDate.getTime()) / 1000;

  return (
    <tr>
      <td>{load.name}</td>
      <td>{formatDuration(seconds)} ago</td>
      <td>{load.pickup_address}</td>
      <td>{load.delivery_address}</td>
      {url === "/" ? (
        <td>
          <Button onClick={onDelete} className="mr-2 mb-2">
            Delete
          </Button>
          <Button onClick={toggleEditModal} className="mr-2 mb-2">
            Edit
          </Button>
          <Button onClick={onPost} className="mr-2 mb-2">
            Post
          </Button>
        </td>
      ) : null}
    </tr>
  );
};

export default Load;
