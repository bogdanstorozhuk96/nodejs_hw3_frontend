import loadStates from "./dataTypes";

const formatDuration = (seconds) => {
  let result = "";
  let timeObj = {};
  let componentCount = 0;

  if (seconds === 0) {
    return "now";
  }
  if (seconds >= 31536000) {
    let years = Math.floor(seconds / 31536000);
    seconds -= years * 31536000;
    timeObj[componentCount] = years > 1 ? `${years} years` : "1 year";
    componentCount++;
  }
  if (seconds >= 86400) {
    let days = Math.floor(seconds / 86400);
    seconds -= days * 86400;
    timeObj[componentCount] = days > 1 ? `${days} days` : "1 day";
    componentCount++;
  }
  if (seconds >= 3600) {
    let hours = Math.floor(seconds / 3600);
    seconds -= hours * 3600;
    timeObj[componentCount] = hours > 1 ? `${hours} hours` : "1 hour";
    componentCount++;
  }
  if (seconds >= 60) {
    let minutes = Math.floor(seconds / 60);
    seconds -= minutes * 60;
    timeObj[componentCount] = minutes > 1 ? `${minutes} minutes` : "1 minute";
    componentCount++;
  }
  if (seconds) {
    timeObj[componentCount] =
      seconds > 1 ? `${Math.floor(seconds)} seconds` : "1 second";
    componentCount++;
  }

  for (let i = 0; i < componentCount; i++) {
    if (componentCount === 1) {
      result += timeObj[i];
    } else if (componentCount - 2 === i) {
      result += `${timeObj[i]} `;
    } else if (componentCount - 1 === i) {
      result += `and ${timeObj[i]}`;
    } else {
      result += `${timeObj[i]}, `;
    }
  }
  return result;
};

const getNextLoadState = (state) => {
  const index = loadStates.findIndex((item) => item === state);
  const newState = loadStates[index + 1];
  return newState;
};

function validateEmail(email) {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

export { formatDuration, getNextLoadState,validateEmail };
