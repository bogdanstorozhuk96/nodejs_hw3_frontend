import styled from "styled-components";

export default styled.h3`
  grid-column: 1/4;
  grid-row: 1/1;
`;
