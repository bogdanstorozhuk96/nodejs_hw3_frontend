import styled from "styled-components";

export default styled.div`
  display: inline-grid;
  width: 100%;
  grid-template-columns: 75% 15% 10%;
  align-items: center;
`;
