import styled from "styled-components";

export default styled.div`
  display: grid;
  grid-template-columns: 25% 25% 25% 25%;
  grid-template-rows: 50px 50px 200px;
  align-items: center;
`;
