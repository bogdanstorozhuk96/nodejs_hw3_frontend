import styled from "styled-components";

export default styled.form`
  grid-column: 1/3;
  grid-row: 3/3;
`;
